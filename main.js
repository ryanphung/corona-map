var geocoder,
    map,
    infowindow,
    nodes = [],
    nodeMaps = {};

const MAP_LOCATIONS = {
  'hcmc': {
    position: [10.7721148, 106.6960897],
    zoom: 12
  },
  'hanoi': {
    position: [21.0227788, 105.8194541],
    zoom: 12
  },
  'vietnam': {
    position: [15.7583199, 101.4133417],
    zoom: 6
  }
}

const INITIAL_LOCATION = 'hcmc'
const INITIAL_POSITION = MAP_LOCATIONS[INITIAL_LOCATION].position
const INITIAL_ZOOM     = MAP_LOCATIONS[INITIAL_LOCATION].zoom

async function initialize() {
  await IMAGINARY.i18n.init({
    queryStringVariable: 'lang',
    translationsDirectory: 'tr',
    defaultLanguage: 'vi'
  })

  geocoder = new google.maps.Geocoder();
  var latlng = new google.maps.LatLng(...INITIAL_POSITION);
  var mapOptions = {
      zoom: INITIAL_ZOOM,
      center: latlng,
      streetViewControl: false
  }
  map = new google.maps.Map(document.getElementById('map'), mapOptions);
  infowindow = new google.maps.InfoWindow();

  google.maps.event.addListener(map, "click", function(event) {
    infowindow.close();
  });

  Tabletop.init({
    key: 'https://docs.google.com/spreadsheets/d/1KfTSCgBJ44wIxONgMC2-Whipva6iot44nZ9utkL0DCo/edit?usp=sharing',
    callback: function(data, tabletop) {
      const allData = data["Confirmed"].all()
      nodes = nodesWithLinks(dedupeLocations(allData.map(parseNode)))
      nodeMaps = getNodesMap(nodes)

      const markers = nodes.map(createMarker).filter(v => v)

      // Path for cluster icons to be appended (1.png, 2.png, etc.)
      // const imagePath = "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m";

      // Enable marker clustering for this map and these markers
      // const markerClusterer = new MarkerClusterer(map, markers, {imagePath: imagePath});
    },
    simpleSheet: false
  })
}

// function codeAddress(address, centering) {
//   geocoder.geocode({
//       'address': address
//   }, function(results, status) {
//     if (status == 'OK') {
//       if (centering !== undefined && centering)
//         map.setCenter(results[0].geometry.location);
//       createMarker(results[0].geometry.location);
//     } else {
//       console.error('Geocode was not successful for the following reason: ' + status);
//     }
//   });
// }

function dedupeLocations(nodes) {
  const nodesMap = {}

  return nodes.map(n => {
    if (isNaN(n.position.lat) || isNaN(n.position.lng))
      return n

    const key = n.position.lat + '' + n.position.lng;
    let newNode = {}

    // if there is another node at the exact location
    if (nodesMap[key]) {
      newNode = {
        ...n,
        position: {
          lat: n.position.lat + Math.random() / 1000,
          lng: n.position.lng + Math.random() / 1000
        }
      }
    }
    else
      newNode = n

    nodesMap[newNode.position.lat + '' + newNode.position.lng] = newNode

    return newNode
  })
}

function parseNode({ latitude, longitude, ...rest }) {
  return {
    position: {
      lat: parseFloat(latitude),
      lng: parseFloat(longitude),
    },
    ...rest
  }
}

function nodesWithLinks(nodes) {
  const nodeMaps = getNodesMap(nodes)
  return nodes.map(({ link, ...n }) => {
    const links = (link || '').split(',').map(parseInt)
    return {
      ...n,
      prevCases: links.map(v => nodeMaps[v]).filter(v => v)
    }
  })
}

function getNodesMap(nodes) {
  return nodes.reduce((s, v) => { s[v.name] = v; return s}, {})
}

STATUS_COLOR = {
  'suspected': 'orange',
  'infected': 'red',
  'discharged': 'blue'
}

function createMarker({ type, status, name, position, address, gender, age, nationality, travelPath, prevCases, source }) {
  var color = STATUS_COLOR[status];

  if (!status || isNaN(position.lat) || isNaN(position.lng))
    return;

  var marker = new google.maps.Marker({
    map,
    position,
    icon: {
      path: "M19,4C11.7,6.5 5.7,12.8 3.5,20.3C0.9,29.4 2.2,33.4 13.7,51.8C21.4,64.1 23.6,69.1 25.2,77.5C25.8,81.1 26.6,84 27,84C27.4,84 28.2,81.1 28.8,77.5C30.4,69.1 32.6,64.1 40.3,51.8C51.9,33.3 53.1,29.4 50.5,20.2C48.2,12.5 41.7,5.9 34.1,3.6C27.3,1.6 25.8,1.6 19,4Z",
      scale: 0.5,
      fillColor: color,
      fillOpacity: 1,
      strokeWeight: 1,
      strokeColor: 'white',
      anchor: new google.maps.Point(27, 86),
      labelOrigin: new google.maps.Point(27, 30)
    },
    label: {
      text: name,
      color: "white"
    }
  });

  // createLines({ position, prevCases })

  marker.addListener('click', function() {
    infowindow.close();
    infowindow.setContent(
      `
      <div style="max-width: 300px">
        <div style="margin-bottom: 4px;">
          <b>${IMAGINARY.i18n.t("case")} ${name}</b> <span style="text-transform:uppercase;">${IMAGINARY.i18n.t(status)}</span><br/>
          ${(gender || age || nationality) ? `<div style="text-transform:uppercase; font-size: .8em">${[IMAGINARY.i18n.t(gender), `${age} ${IMAGINARY.i18n.t('years old')}`, IMAGINARY.i18n.t(nationality.toLowerCase())].filter(v => v).join(', ')}</div>` : ''}
        </div>
        ${address ? `<div style="margin-bottom: 4px;"><span style="text-decoration: underline">${IMAGINARY.i18n.t('address')}:</span> ${address}</div>` : ''}
        ${travelPath ? `<div style="margin-bottom: 4px;"><span style="text-decoration: underline">${IMAGINARY.i18n.t('travel path')}:</span> ${travelPath}</div>` : ''}
        ${(prevCases && prevCases.length) ? `<div style="margin-bottom: 4px;"><span style="text-decoration: underline">${IMAGINARY.i18n.t('infected by')}:</span> ${prevCases.map(v => v.name).join(', ')}</div>` : ''}
        ${source ? `<div style="margin-bottom: 4px;"><span style="text-decoration: underline">${IMAGINARY.i18n.t('source')}:</span> <a href=${source} target="_blank">${IMAGINARY.i18n.t('click here')}</a></div>`: ''}
      </div>
      `
    );
    infowindow.open(map, marker);
  });

  return marker
}

function createLines({ position, prevCases }) {
  prevCases.map(v => ([
    position,
    v.position
  ])).map(coordinate => {
    const path = new google.maps.Polyline({
      path: coordinate,
      icons: [{
        icon: {
          path: google.maps.SymbolPath.BACKWARD_OPEN_ARROW,
          scale: 2
        },
        offset: '20px'
      }],
      geodesic: true,
      strokeColor: 'black',
      strokeOpacity: .5,
      strokeWeight: 1
    })
    path.setMap(map)
  })
}
